Wie alles begann
================

Motivation
----------

Immer wenn ich mal wieder ein Access-Projekt habe, stellt sich mir das
gleiche Problem: Die Aktionen, die ich vor dem *Deployment* machen muss,
nerven mich gewaltig. Jedesmal das gleiche Spiel:

-  Testdaten entfernen
-  Versionsnummer in Applikation und Setup-Script aktualisieren
-  Alles mit OASIS exportieren
-  Diese letzten Änderungen in der Quellcodeverwaltung committen
-  Datenbank komprimieren oder .accde erstellen
-  Setup laufen lassen
-  Hochladen in die Cloud

Hinzu kommt das Problem, dass ich irgendwie gern sicherstellen würde,
dass mein Code in der Quellcodeverwaltung auch tatsächlich mit dem
übereinstimmt, den ich während der Entwicklung erstellt habe. Denn wenn
ich zufälligerweise vergessen haben sollte, diese eine wichtige Änderung
einzuchecken, dann kann ich mir später einen Wolf suchen, wenn der Kunde
einen Bug meldet, den ich in *meinem* Code nicht nachvollziehen kann.

Wenn ich diese Schritte automatisieren könnte, wäre mir eine große Last
von den Schultern genommen. Und wenn ich dabei sicherstellen könnte,
dass der *Build* tatsächlich aus der Quellcodeverwaltung heraus erfolgt,
dann wäre ich doppelt beruhigt.

Auch wenn ich die Abschlussarbeiten für das *Final Release* vielleicht
doch von Hand durchführe: Zumindest für die häufigere Bereitstellung der
Testversion für interessierte Anwender hilft mir das, mich auf das
Wesentliche zu konzentrieren: Die Entwicklung.

So entstand die Idee für ein **Access-Make** (der Name ist momentan noch
ein Arbeitstitel).

Im Laufe der Zeit
-----------------

   "I am so clever that sometimes I don’t understand a single word of
   what I am saying." - Oscar Wilde

So ging es mir im Verlauf dieses Projektes mehrfach. Irgendwie habe ich
immer zwischen den Stühlen gesessen. Sollte ich *AccessMake* mit Access
oder mit Python programmieren? Python kann eleganter mit
Stringmanipulationen umgehen, Access ist gerade als Unterstützung bei
Access-nahen Aktionen irgendwie “naheliegender”.

Die im vorigen Kapitel geschilderte ursprüngliche Idee hat also im Laufe
der Zeit einige Änderungen erfahren. Inzwischen bin ich wieder bei
Python angelangt, und das hatte folgende Gründe.

Zum Einen habe ich in `GitLab <https://gitlab.com>`__ ein CI-System
gefunden, das Windows unterstützt. Es tut dies in der Form, dass ein
sog. “Runner” unter Windows läuft. Dieser nimmt die Aufträge des
CI-Systems entgegen und führt sie aus. Dadurch fielen einige
organisatorische Arbeiten weg, die nun von Gitlabs “Pipelines”
übernommen werden.

Dann habe ich auf der
`AEK20 <http://www.donkarl.com/AEK/Archiv/AEK20.htm>`__ mit Bernd
Gilles, dem Entwickler von OASIS, gesprochen. Dieser erklärte mir, dass
OASIS mittels eines einfachen Kommandozeilenbefehls direkt aus dem
Quellcode eine .accdb oder .accde erstellen kann. Damit fielen auch
diese Aktionen weg, die in Python etwas instabil liefen.

In Anbetracht der direkten Erzeugung der .accde aus dem Quellcode war
dann auch die Ausführung von Löschcode für die Testdaten nicht mehr
notwendig. Was gar nicht erst in Git enthalten ist, wird auch nicht in
die .accde-Datei kommen und braucht daher auch nicht gelöscht zu werden.

Und letztlich konnte auch das “Hochladen zum Kunden” von dem CI-System
übernommen werden, es nennt sich dort “collect artifacts”.

Damit blieb vor dem Aufbau der .accde-Datei und dem Compilieren des
Setups nur noch ein wenig Textverarbeitung übrig:

-  Versionsnummer aus dem Quellcode auslesen und in Setup-Script patchen
-  aktuellen Git-Hash ermitteln und in Quellcode und Setup-Script
   patchen
-  aktuelles Datum ermitteln, als ISO-Date formatieren und in Quellcode
   und Setup-Script patchen

Und dafür ist Python hervorragend geeignet.
